<?php
require_once "../../model/BindParam.php";
require_once "../../model/query_database.php";

class nosotros_controller
{

    public function prueba(){
        $query = "INSERT INTO `prueba` (`id`, `nombre`) VALUES (?, ?);";

        $binParam = new BindParam();

        $binParam->add('i', 3);
        $binParam->add('i', 3333);

        return json_encode(query_database::delete_update_insert($query, $binParam));
    }

}